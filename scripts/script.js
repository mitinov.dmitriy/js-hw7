window.addEventListener("load", function () {
  function clearAll(seconds) {
    let allMarkup = document.body;
    let secondsLeft = seconds + 1;
    let blockToDisplay = document.createElement("div");
    blockToDisplay.style.cssText =
      "width: 350px; height: 50px; background-color: green; color: white; display: flex; align-items: center; /" +
      "font-size: 13pt";
    let timeInterval = setInterval(() => {
      document.body.prepend(blockToDisplay);
      blockToDisplay.innerHTML = `All content will be deleted in ${--secondsLeft}`;
      if (secondsLeft === 0) {
        clearInterval(timeInterval);
        while (allMarkup.firstElementChild) {
          allMarkup.firstElementChild.remove();
        }
      }
    }, 1000);
  }

  function showElementsList(array, domElem = document.body) {
    if (!Array.isArray(array)) {
      console.error("First argument must be an Array");
      return;
    } else if (array.length === 0) {
      console.error("Given Array length is 0");
      return;
    }
    let elemList = document.createElement("ul");

    array.map((elem) => {
      let listItem = document.createElement("li");
      if (Array.isArray(elem)) {
        showElementsList(elem, elemList);
      } else {
        listItem.innerHTML = `${elem}`;
        elemList.append(listItem);
      }
    });
    domElem.append(elemList);
  }

  showElementsList([
    "Kharkiv",
    "Kiev",
    ["Borispol", "Irpin"],
    "Odessa",
    "Lviv",
    "Dnieper",
  ]);
  clearAll(3);
});
